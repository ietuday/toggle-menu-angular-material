import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  position = 'above';
  togglePosition() {
    this.position = this.position == 'above' ? 'below' :'above';
  }
}
